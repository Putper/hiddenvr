﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
	public GameObject player;
	public GameObject feet;
	private AudioSource audio_source;
	new private Collider collider;
	private AudioClip current_clip;


	private void Awake()
	{
		audio_source = feet.GetComponent<AudioSource>();
		collider = player.GetComponent<Collider>();
	}


	private void FixedUpdate()
	{
		if(!audio_source.isPlaying)
		{
			RaycastHit hit;

			Ray ray = new Ray(feet.transform.position, Vector3.down);
			// if(collider.Raycast(ray, out hit, 10f))
			if(Physics.Raycast(feet.transform.position, Vector3.down, out hit, 0.2f))
			{
				Debug.DrawLine(feet.transform.position, hit.transform.position);
				// Debug.Log("hit");
				if(hit.collider.gameObject.GetComponent<Floor>() != null)
				{
					AudioClip[] floor_clips = hit.collider.gameObject.GetComponent<Floor>().footsteps;
					int index = Random.Range(0, floor_clips.Length);
					if(floor_clips[index] != current_clip)
					{
						current_clip = floor_clips[index];
						audio_source.clip = current_clip;
						audio_source.Play();
					}
				}
			}
			else
			{
				audio_source.clip = null;
				current_clip = null;
			}
		}
	}
}
