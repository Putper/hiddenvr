﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingCharacterController : MonoBehaviour
{
	public float movement_speed = 5.0f;
	public float jump_height = 5.0f;
	new public Camera camera;
	public float mouse_sensitivity = 10.0f;
	public Vector2 pitch_min_max = new Vector2(-60, 60);	// How high and low you are allowed to look

	private Vector3 gravity = Vector3.zero;
	private CharacterController character_controller;
	private float yaw;	// y rotation
	private float pitch;	// x rotation
	private Vector3 rotation_smooth_velocity;
	private Vector3 current_rotation;


	private void Awake()
	{
		character_controller = GetComponent<CharacterController>();
	}


	private void FixedUpdate()
	{
		// Set movement based on user input
		Vector3 movement = camera.transform.TransformDirection (
			new Vector3 (
				Input.GetAxis("Horizontal"),
				0,
				Input.GetAxis("Vertical")
			)
		) * movement_speed;
		movement.y = 0;

		// If character is in the air, apply gravity
		if( !character_controller.isGrounded )
			gravity += Physics.gravity * Time.deltaTime;

		// If character is on a surface
		else
		{
			gravity = Vector3.zero;

			// Character Jump
			if( Input.GetKey(KeyCode.Space) )
				gravity.y += jump_height;
		}

		movement += gravity;	// Apply gravity
		character_controller.Move(movement * Time.fixedDeltaTime);	// Apply character movement
	}

	
	private void LateUpdate()
	{
		yaw += Input.GetAxis("Mouse X") * mouse_sensitivity;
		pitch -= Input.GetAxis("Mouse Y") * mouse_sensitivity;
		pitch = Mathf.Clamp( pitch, pitch_min_max.x, pitch_min_max.y );

		camera.transform.localEulerAngles = new Vector3(pitch, yaw, 0);	// rotate camera
		this.transform.eulerAngles = new Vector3(0, yaw, 0);
	}
}
