﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
	// ranges from 0-3. Where 3 is loudest.
	// 3: Loudest. (Metal, ceramic tiles, gravel)
	// 2: Silent except when running or jumping. hearable from up close unless sneaking. (Stone, Wood)
	// 1: Silent except when jumping. (Grass, Carpet)
	// 0: Completely silent. (Moss)
	public int loudness = 0;
	public AudioClip[] footsteps;	// Audio for when walking on the floor
	public AudioClip jump;	// Audio for when landing on the floor
}
