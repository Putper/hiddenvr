﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowDetection : MonoBehaviour
{
	public GameObject player;	// Player
	// public float player_visibility = 0;
	private Light[] lights;	// list of all lights in the game

	private void Start()
	{
		lights = FindObjectsOfType<Light>();	// Store all lights
	}

	
	private void FixedUpdate()
	{
		Debug.Log(player_visibility());
	}


	// Visibility of player (ranges from 0 to 1 where 0 is in complete darkness and 1 in full light)
	public float player_visibility()
	{
		float visibility = 0;

		// for each light in the scene
		foreach(Light light in lights)
		{
			// Get direction to player
			Vector3 direction = player.transform.position - light.gameObject.transform.position;

			// Debug.Log(direction);

			if(light.type == LightType.Spot)
			{
				// if(direction == Vector3.)
				// return;
			}

			RaycastHit hit;

			// Cast a raycast towards the player, with the length of the range of the light
			if(Physics.Raycast(light.gameObject.transform.position, direction, out hit, light.range))
			{
				// If it hits the player
				if(hit.transform == player.transform)
				{
					float distance = Vector3.Distance(light.gameObject.transform.position, player.transform.position); // Calculate distance to the player
					float visibility_this_light = 100/light.range*(light.range-distance) / 100;	// Get percentage of how far the player is in the light
					if(visibility_this_light > visibility)	// If it's the highest visibility number, store it
						visibility = visibility_this_light;
				}
			}
			// Debug.Log(visibility);
		}

		return visibility;
	}
}
